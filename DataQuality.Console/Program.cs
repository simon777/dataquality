﻿using System.Diagnostics;
using DataValidation.Dal;
using DataValidation.Dal.Engine;

namespace DataValidation.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            System.Console.WriteLine("Start:");
            stopwatch.Start();
           // TestData.InsertTestData();  //Insert Test Data 
            DataOperationEngine.Run(); 
            stopwatch.Stop();
            System.Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
            System.Console.ReadLine(); 
        }
    }
}