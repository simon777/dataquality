﻿using System.Collections.Generic;
using System.Linq;
using DataValidation.Dal.Entities.DataOperation;
using DataValidation.Dal.Enum;
using DataValidation.Dal.Operations;

namespace DataValidation.Dal.Factory
{
    public static class DataOperationFactory
    {
        public static IDataOperation GetReadOperation(DataType dataType, long sourceId)
        {
            switch (dataType)
            {
                case DataType.Measurement:
                    return new ReadMeasurementDataOperation(sourceId);
                case DataType.Product:
                    return new ReadMeasurementDataOperation(sourceId);
                case DataType.Weather:
                    return new ReadMeasurementDataOperation(sourceId);
                default:
                    return null;
            }
        }

        public static IDataOperation GetValidationOperation(string operationName, IEnumerable<DataOperationMethodParameterData> parameters)
        {
            switch (operationName)
            {
                case "MissingData":
                    return new MissingDataOperation();
                case "RecurringData":
                    int? numberOfRepetitions =
                        parameters.Where(parameter => parameter.DataOperationMethodParameter.Name.Equals("Recurring")).Select(parameter => parameter.DataInteger).FirstOrDefault();
                    if (numberOfRepetitions != null)
                        return new RecurringDataOperation((int) numberOfRepetitions);
                    return null;
                case "OutOfRangeData":
                    decimal? min =
                        parameters.Where(parameter => parameter.DataOperationMethodParameter.Name.Equals("Min")).Select(parameter => parameter.DataDecimal).FirstOrDefault();
                    decimal? max =
                        parameters.Where(parameter => parameter.DataOperationMethodParameter.Name.Equals("Max")).Select(parameter => parameter.DataDecimal).FirstOrDefault();
                    if (min != null && max != null)
                        return new OutOfRangeDataOperation((decimal) min, (decimal) max);
                    return null;
                default:
                    return null;
            }
        }
    }
}