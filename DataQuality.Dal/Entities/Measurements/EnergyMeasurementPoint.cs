﻿using System.ComponentModel.DataAnnotations;

namespace DataValidation.Dal.Entities.Measurements
{
    public class EnergyMeasurementPoint : MeasurementPoint 
    {
        [StringLength(50)]
        public string Smm { get; set; }
        [StringLength(150)]
        public string Address { get; set; }
        public bool IsMeasured { get; set; }
        public string  Region { get; set; }
        public int ExpectedYearlyLoadEt { get; set; }


        //public long? PartnerId { get; set; }
        //public virtual Partner Partner { get; set; }

        public long EnergyMeasurementPointTypeId { get; set; }
        public virtual EnergyMeasurementPointType EnergyMeasurementPointType { get; set; }
    }
}