﻿using System.ComponentModel.DataAnnotations;
using DataValidation.Dal.Entities.Base;

namespace DataValidation.Dal.Entities.Measurements
{
    public class EnergyMeasurementPointType : Entity
    {
        [StringLength(255)]
        public string Title { get; set; }
        public bool IsProduction { get; set; }
    }
}