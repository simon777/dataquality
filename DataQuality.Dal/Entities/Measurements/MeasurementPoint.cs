﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataValidation.Dal.Entities.Base;
using DataValidation.Dal.Entities.DataOperation;

namespace DataValidation.Dal.Entities.Measurements
{
    public abstract class MeasurementPoint : Entity
    {
        protected MeasurementPoint()
        {
            MeasurementPointDataOperationGroups = new HashSet<DataOperationGroup>();
        }

        [StringLength(250)]
        public string Title { get; set; }
        public virtual ICollection<DataOperationGroup> MeasurementPointDataOperationGroups { get; set; }
    }
}