﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataValidation.Dal.Entities.Datas
{
    public class MeasurementDataBase : Data
    {
        public int? UserId { get; set; }

        [Index]
        public long MeasurementPointId
        {
            get { return this.SourceId;}
            set { SourceId = value; }
        }
    }
}