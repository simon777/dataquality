﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataValidation.Dal.Entities.Datas
{
    public class ProductDataBase : Data
    {
        public bool Locked { get; set; }
        public int? UserId { get; set; }
        [Index]
        public long DataSourceId
        {
            get { return this.SourceId; }
            set { SourceId = value; }
        }
    }
}