﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Entities.Datas
{
    public class Data
    {
        public long Id { get; set; }
        public DateTime UtcTimeStamp { get; set; }
        public int Revision { get; set; }
        public decimal? Value { get; set; }
        public DateTime? DateInserted { get; set; }
        public virtual Status DataStatus { get; set; }
        [NotMapped]
        public long SourceId { get; set; }
    }
}