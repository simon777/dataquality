﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataValidation.Dal.Entities.Datas
{
    public class WeatherDataBase : Data
    {
        [Index]
        public long WeatherSourceId
        {
            get { return this.SourceId; }
            set { SourceId = value; }
        }
    }
}