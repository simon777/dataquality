﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataValidation.Dal.Entities.Base
{
    /// <summary>
    ///     Base class for entities
    /// </summary>
    public abstract class Entity: IValidatableObject
    {
        protected Entity()
        {
            IsActive = true;
            IsDeleted = false;
        }

        #region Members

        private int? _requestedHashCode;

        #endregion

        #region Properties

        public long Id { get;  set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public long? ExternalId { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; private set; }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Check if this entity is transient, ie, without identity at this moment
        /// </summary>
        /// <returns>True if entity is transient, else false</returns>
        public bool IsTransient()
        {
            return this.Id == default(long);
        }

        /// <summary>
        ///     Change current identity for a new non transient identity
        /// </summary>
        /// <param name="identity">the new identity</param>
        public void ChangeCurrentIdentity(long identity)
        {
            if (identity != default(long))
                this.Id = identity;
        }

        public void Delete()
        {
            this.IsDeleted = true;
        }

        public void Deactivate()
        {
            this.IsActive = false;
        }

        public void Activate()
        {
            this.IsActive = true;
        }

        public void UnDelete()
        {
            this.IsDeleted = false;
        }

        #endregion

        #region Overrides Methods

        /// <summary>
        ///     <see cref="M:System.Object.Equals" />
        /// </summary>
        /// <param name="obj">
        ///     <see cref="M:System.Object.Equals" />
        /// </param>
        /// <returns>
        ///     <see cref="M:System.Object.Equals" />
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Entity))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            var item = (Entity) obj;

            if (item.IsTransient() || this.IsTransient())
                return false;
            return item.Id == this.Id;
        }

        /// <summary>
        ///     <see cref="M:System.Object.GetHashCode" />
        /// </summary>
        /// <returns>
        ///     <see cref="M:System.Object.GetHashCode" />
        /// </returns>
        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.Id.GetHashCode() ^ 31;
                        // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _requestedHashCode.Value;
            }
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified object is valid.
        /// </summary>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        /// <param name="validationContext">The validation context.</param>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            return result;
        }

        public static bool operator ==(Entity left, Entity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null)) ? true : false;
            return left.Equals(right);
        }

        public static bool operator !=(Entity left, Entity right)
        {
            return !(left == right);
        }

        #endregion
    }
}