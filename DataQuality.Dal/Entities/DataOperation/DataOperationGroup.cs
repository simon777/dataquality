﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataValidation.Dal.Entities.Base;
using DataValidation.Dal.Entities.Measurements;

namespace DataValidation.Dal.Entities.DataOperation
{
    public class DataOperationGroup : Entity
    {
        public DataOperationGroup()
        {
            DataOperations = new HashSet<DataOperation>();
            MeasurementPoints = new HashSet<MeasurementPoint>();
        }

        [StringLength(128)]
        public string Title { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public bool DefaultExecute { get; set; }

        public virtual ICollection<DataOperation> DataOperations { get; set; }
        public virtual ICollection<MeasurementPoint> MeasurementPoints { get; set; } 
    }
}