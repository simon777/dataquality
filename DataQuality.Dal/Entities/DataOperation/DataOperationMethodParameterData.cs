﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataValidation.Dal.Entities.Base;

namespace DataValidation.Dal.Entities.DataOperation
{
    public class DataOperationMethodParameterData : Entity
    {
        public long DataOperationId { get; set; }
        public long DataOperationMethodParameterId { get; set; }
        public int? DataInteger { get; set; }
        public DateTime? DataDateTime { get; set; }
        public decimal? DataDecimal { get; set; }
        public bool? DataBit { get; set; }

        [StringLength(500)]
        public string DataString { get; set; }

        [ForeignKey("DataOperationMethodParameterId")]
        public virtual DataOperationMethodParameter DataOperationMethodParameter { get; set; }

        [ForeignKey("DataOperationId")]
        public virtual DataOperation DataOperation { get; set; }
    }
}