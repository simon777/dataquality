﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataValidation.Dal.Entities.Base;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Entities.DataOperation
{
    public class DataOperationMethod : Entity
    {
        public DataOperationMethod()
        {
            Parameters = new HashSet<DataOperationMethodParameter>();
        }

        [StringLength(150)]
        public string Title { get; set; }
        public string Name { get; set; }

        public int ExecuteOrder { get; set; }
        [NotMapped]
        public DataOperationType OperationType
        {
            get { return (DataOperationType)System.Enum.Parse(typeof(DataOperationType), OperationTypeString ?? "Undefined"); }
            internal set { OperationTypeString = System.Enum.GetName(typeof(DataOperationType), value); }
        }
        public string OperationTypeString { get; set; }
        public virtual  ICollection<DataOperationMethodParameter> Parameters { get; set; }
    }
}