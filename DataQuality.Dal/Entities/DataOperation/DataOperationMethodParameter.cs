﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataValidation.Dal.Entities.Base;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Entities.DataOperation
{
    public class DataOperationMethodParameter : Entity
    {
        public long DataOperationMethodId { get; set; }

        [StringLength(150)]
        public string Title { get; set; }
        public string Name { get; set; }

        [ForeignKey("DataOperationMethodId")]
        public virtual DataOperationMethod DataOperationMethod { get; set; }

        [NotMapped]
        public ParameterType ParameterType
        {
            get { return (ParameterType)System.Enum.Parse(typeof(ParameterType), ParameterTypeString ?? "String"); }
            internal set { ParameterTypeString = System.Enum.GetName(typeof(ParameterType), value); }
        }
        [StringLength(50)]
        public string ParameterTypeString { get; set; }
    }
}