﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataValidation.Dal.Entities.Base;

namespace DataValidation.Dal.Entities.DataOperation
{
    public class DataOperation : Entity
    {
        public DataOperation()
        {
            ParameterDatas = new HashSet<DataOperationMethodParameterData>();
        }

        public long DataOperationGroupId { get; set; }
        public long DataOperationMetodId { get; set; }

        [ForeignKey("DataOperationGroupId")]
        public virtual DataOperationGroup DataOperationGroup { get; set; }

        [ForeignKey("DataOperationMetodId")]
        public virtual DataOperationMethod DataOperationMethod { get; set; }

        public virtual ICollection<DataOperationMethodParameterData> ParameterDatas { get; set; }
    }
}