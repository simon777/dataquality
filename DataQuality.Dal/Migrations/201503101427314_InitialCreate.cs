namespace DataValidation.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DataOperationGroups",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 128),
                        Description = c.String(maxLength: 512),
                        DefaultExecute = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataOperations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataOperationGroupId = c.Long(nullable: false),
                        DataOperationMetodId = c.Long(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataOperationGroups", t => t.DataOperationGroupId, cascadeDelete: true)
                .ForeignKey("dbo.DataOperationMethods", t => t.DataOperationMetodId)
                .Index(t => t.DataOperationGroupId)
                .Index(t => t.DataOperationMetodId);
            
            CreateTable(
                "dbo.DataOperationMethods",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 150),
                        Name = c.String(),
                        ExecuteOrder = c.Int(nullable: false),
                        OperationTypeString = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataOperationMethodParameters",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataOperationMethodId = c.Long(nullable: false),
                        Title = c.String(maxLength: 150),
                        Name = c.String(),
                        ParameterTypeString = c.String(maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataOperationMethods", t => t.DataOperationMethodId, cascadeDelete: true)
                .Index(t => t.DataOperationMethodId);
            
            CreateTable(
                "dbo.DataOperationMethodParameterDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DataOperationId = c.Long(nullable: false),
                        DataOperationMethodParameterId = c.Long(nullable: false),
                        DataInteger = c.Int(),
                        DataDateTime = c.DateTime(),
                        DataDecimal = c.Decimal(precision: 18, scale: 2),
                        DataBit = c.Boolean(),
                        DataString = c.String(maxLength: 500),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataOperations", t => t.DataOperationId, cascadeDelete: true)
                .ForeignKey("dbo.DataOperationMethodParameters", t => t.DataOperationMethodParameterId, cascadeDelete: true)
                .Index(t => t.DataOperationId)
                .Index(t => t.DataOperationMethodParameterId);
            
            CreateTable(
                "dbo.MeasurementPoints",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 250),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EnergyMeasurementPointTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(maxLength: 255),
                        IsProduction = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ExternalId = c.Long(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeasurementDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Int(),
                        MeasurementPointId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MeasurementPointId);
            
            CreateTable(
                "dbo.MeasurementDataRaws",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        UserId = c.Int(),
                        MeasurementPointId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MeasurementPointId);
            
            CreateTable(
                "dbo.ProductDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Locked = c.Boolean(nullable: false),
                        UserId = c.Int(),
                        DataSourceId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.ProductDataRaws",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Locked = c.Boolean(nullable: false),
                        UserId = c.Int(),
                        DataSourceId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.DataSourceId);
            
            CreateTable(
                "dbo.WeatherDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        WeatherSourceId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.WeatherSourceId);
            
            CreateTable(
                "dbo.WeatherDataRaws",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        WeatherSourceId = c.Long(nullable: false),
                        UtcTimeStamp = c.DateTime(nullable: false),
                        Revision = c.Int(nullable: false),
                        Value = c.Decimal(precision: 18, scale: 2),
                        DateInserted = c.DateTime(),
                        DataStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.WeatherSourceId);
            
            CreateTable(
                "dbo.MeasurementPointDataOperationGroups",
                c => new
                    {
                        MeasurementPoint_Id = c.Long(nullable: false),
                        DataOperationGroup_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.MeasurementPoint_Id, t.DataOperationGroup_Id })
                .ForeignKey("dbo.MeasurementPoints", t => t.MeasurementPoint_Id, cascadeDelete: true)
                .ForeignKey("dbo.DataOperationGroups", t => t.DataOperationGroup_Id, cascadeDelete: true)
                .Index(t => t.MeasurementPoint_Id)
                .Index(t => t.DataOperationGroup_Id);
            
            CreateTable(
                "dbo.MeasurementPointsEnergy",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Smm = c.String(maxLength: 50),
                        Address = c.String(maxLength: 150),
                        IsMeasured = c.Boolean(nullable: false),
                        Region = c.String(),
                        ExpectedYearlyLoadEt = c.Int(nullable: false),
                        EnergyMeasurementPointTypeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeasurementPoints", t => t.Id)
                .ForeignKey("dbo.EnergyMeasurementPointTypes", t => t.EnergyMeasurementPointTypeId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.EnergyMeasurementPointTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MeasurementPointsEnergy", "EnergyMeasurementPointTypeId", "dbo.EnergyMeasurementPointTypes");
            DropForeignKey("dbo.MeasurementPointsEnergy", "Id", "dbo.MeasurementPoints");
            DropForeignKey("dbo.MeasurementPointDataOperationGroups", "DataOperationGroup_Id", "dbo.DataOperationGroups");
            DropForeignKey("dbo.MeasurementPointDataOperationGroups", "MeasurementPoint_Id", "dbo.MeasurementPoints");
            DropForeignKey("dbo.DataOperationMethodParameterDatas", "DataOperationMethodParameterId", "dbo.DataOperationMethodParameters");
            DropForeignKey("dbo.DataOperationMethodParameterDatas", "DataOperationId", "dbo.DataOperations");
            DropForeignKey("dbo.DataOperations", "DataOperationMetodId", "dbo.DataOperationMethods");
            DropForeignKey("dbo.DataOperationMethodParameters", "DataOperationMethodId", "dbo.DataOperationMethods");
            DropForeignKey("dbo.DataOperations", "DataOperationGroupId", "dbo.DataOperationGroups");
            DropIndex("dbo.MeasurementPointsEnergy", new[] { "EnergyMeasurementPointTypeId" });
            DropIndex("dbo.MeasurementPointsEnergy", new[] { "Id" });
            DropIndex("dbo.MeasurementPointDataOperationGroups", new[] { "DataOperationGroup_Id" });
            DropIndex("dbo.MeasurementPointDataOperationGroups", new[] { "MeasurementPoint_Id" });
            DropIndex("dbo.WeatherDataRaws", new[] { "WeatherSourceId" });
            DropIndex("dbo.WeatherDatas", new[] { "WeatherSourceId" });
            DropIndex("dbo.ProductDataRaws", new[] { "DataSourceId" });
            DropIndex("dbo.ProductDatas", new[] { "DataSourceId" });
            DropIndex("dbo.MeasurementDataRaws", new[] { "MeasurementPointId" });
            DropIndex("dbo.MeasurementDatas", new[] { "MeasurementPointId" });
            DropIndex("dbo.DataOperationMethodParameterDatas", new[] { "DataOperationMethodParameterId" });
            DropIndex("dbo.DataOperationMethodParameterDatas", new[] { "DataOperationId" });
            DropIndex("dbo.DataOperationMethodParameters", new[] { "DataOperationMethodId" });
            DropIndex("dbo.DataOperations", new[] { "DataOperationMetodId" });
            DropIndex("dbo.DataOperations", new[] { "DataOperationGroupId" });
            DropTable("dbo.MeasurementPointsEnergy");
            DropTable("dbo.MeasurementPointDataOperationGroups");
            DropTable("dbo.WeatherDataRaws");
            DropTable("dbo.WeatherDatas");
            DropTable("dbo.ProductDataRaws");
            DropTable("dbo.ProductDatas");
            DropTable("dbo.MeasurementDataRaws");
            DropTable("dbo.MeasurementDatas");
            DropTable("dbo.EnergyMeasurementPointTypes");
            DropTable("dbo.MeasurementPoints");
            DropTable("dbo.DataOperationMethodParameterDatas");
            DropTable("dbo.DataOperationMethodParameters");
            DropTable("dbo.DataOperationMethods");
            DropTable("dbo.DataOperations");
            DropTable("dbo.DataOperationGroups");
        }
    }
}
