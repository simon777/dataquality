using System.Collections.Generic;
using System.Data.Entity.Migrations;
using DataValidation.Dal.Entities.DataOperation;
using DataValidation.Dal.Entities.Measurements;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
            var outOfRangeMethod = new DataOperationMethod
            {
                Id = 1,
                Title = "Out of Range Method",
                ExecuteOrder = 1,
                Name = "OutOfRangeData",
                OperationType = DataOperationType.Validation,
                Parameters = new List<DataOperationMethodParameter>
                {
                    new DataOperationMethodParameter
                    {
                        Id = 1,
                        Name = "Min",
                        Title = "Min Value",
                        ParameterType = ParameterType.Decimal
                    },
                    new DataOperationMethodParameter
                    {
                        Id = 2,
                        Name = "Max",
                        Title = "Max Value",
                        ParameterType = ParameterType.Decimal
                    }
                }
            };
            var recurringMethod = new DataOperationMethod
            {
                Id = 2,
                Title = "Recurring Data",
                ExecuteOrder = 2,
                Name = "RecurringData",
                OperationType = DataOperationType.Validation,
                Parameters = new List<DataOperationMethodParameter>
                {
                    new DataOperationMethodParameter
                    {
                        Id = 3,
                        Name = "Recurring",
                        Title = "Min number of recurring datas",
                        ParameterType = ParameterType.Integer
                    },
                }
            };
            var missingMethod = new DataOperationMethod
            {
                Id = 3,
                Title = "Out of Range Method",
                ExecuteOrder = 3,
                Name = "MissingData",
                OperationType = DataOperationType.Validation,
            };
            context.DataOperationMethods.Add(outOfRangeMethod);
            context.DataOperationMethods.Add(recurringMethod);
            context.DataOperationMethods.Add(missingMethod);
            var dataOperationGroup = new DataOperationGroup
            {
                Title = "Default Group",
                Description = "This is deafult Group",
                DefaultExecute = true,
                DataOperations = new List<DataOperation>
                {
                    new DataOperation
                    {
                        DataOperationMetodId = 1,
                        ParameterDatas = new List<DataOperationMethodParameterData>
                        {
                            new DataOperationMethodParameterData
                            {
                                DataDecimal = 10,
                                DataOperationMethodParameterId = 1
                            },
                            new DataOperationMethodParameterData
                            {
                                DataDecimal = 50,
                                DataOperationMethodParameterId = 2
                            }
                        }
                    },
                    new DataOperation
                    {
                        DataOperationMetodId = 2,
                        ParameterDatas = new List<DataOperationMethodParameterData>
                        {
                            new DataOperationMethodParameterData
                            {
                                DataInteger = 4,
                                DataOperationMethodParameterId = 3
                            }
                        }
                    },
                    new DataOperation
                    {
                        DataOperationMetodId = 3,
                    }
                }
            };
            context.DataOperationGroups.Add(dataOperationGroup);
            var energyMeasurementPointType = new EnergyMeasurementPointType
            {
                Title = "EnergyPointType",
                IsProduction = true,
            };
            var measurementPoints = new List<EnergyMeasurementPoint>
            {
                new EnergyMeasurementPoint
                {
                    Title = "Elektrarna Ljubljana",
                    Smm = "26356",
                    Address = "Ljubljana",
                    EnergyMeasurementPointType = energyMeasurementPointType,
                    MeasurementPointDataOperationGroups = new List<DataOperationGroup>
                    {
                        dataOperationGroup
                    }
                },
                new EnergyMeasurementPoint
                {
                    Title = "Elektrarna Celje",
                    Smm = "54754",
                    Address = "Celje",
                    EnergyMeasurementPointType = energyMeasurementPointType,
                    MeasurementPointDataOperationGroups = new List<DataOperationGroup>
                    {
                        dataOperationGroup
                    }
                },
                new EnergyMeasurementPoint
                {
                    Title = "Elektrarna Koper",
                    Smm = "56546",
                    Address = "Koper",
                    EnergyMeasurementPointType = energyMeasurementPointType,
                    MeasurementPointDataOperationGroups = new List<DataOperationGroup>
                    {
                        dataOperationGroup
                    }
                },
                new EnergyMeasurementPoint
                {
                    Title = "Elektrarna Maribor",
                    Smm = "56546",
                    Address = "Maribor",
                    EnergyMeasurementPointType = energyMeasurementPointType,
                    MeasurementPointDataOperationGroups = new List<DataOperationGroup>
                    {
                        dataOperationGroup
                    }
                }
            };
            context.EnergyMeasurementPoints.AddRange(measurementPoints);
            base.Seed(context);
        }
    }
}