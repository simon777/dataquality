﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataValidation.Dal.Entities;
using DataValidation.Dal.Entities.Datas;

namespace DataValidation.Dal.Operations
{
    public interface IDataOperation
    {
        List<Data> Execute(List<Data> datas); 
    }
}
