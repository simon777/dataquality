﻿using System.Collections.Generic;
using System.Linq;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Operations
{
    public class OutOfRangeDataOperation : IDataOperation
    {
        private readonly decimal _maxValue;
        private readonly decimal _minValue;

        public OutOfRangeDataOperation(decimal minValue, decimal maxValue)
        {
            _minValue = minValue;
            _maxValue = maxValue;
        }

        public List<Data> Execute(List<Data> datas)
        {
            datas.Where(data => data.Value < _minValue || data.Value > _maxValue).ToList().ForEach(data => data.DataStatus = Status.Napaka);
            return datas;
        }
    }
}