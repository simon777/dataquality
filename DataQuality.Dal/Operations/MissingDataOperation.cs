﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataValidation.Dal.Entities;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Operations
{
    public class MissingDataOperation : IDataOperation
    {
        public List<Data> Execute(List<Data> datas)
        {
            DateTime nexUtcDateTime = datas.First().UtcTimeStamp;
            List<Data> missingDatas = new List<Data>();
            foreach (Data data in datas)
            {
                if (!data.UtcTimeStamp.Equals(nexUtcDateTime))
                {
                    do
                    {
                        missingDatas.Add(new Data()
                        {
                            UtcTimeStamp = nexUtcDateTime,
                            DataStatus = Status.Manjkajoc,
                            Value = 0 
                        });
                        nexUtcDateTime = nexUtcDateTime.AddMinutes(15);
                    } while (!data.UtcTimeStamp.Equals(nexUtcDateTime));
                }

                nexUtcDateTime = nexUtcDateTime.AddMinutes(15);
            }
            datas.AddRange(missingDatas);
            return datas.OrderBy(data => data.UtcTimeStamp).ToList(); 
        }
    }
}