﻿using System.Collections.Generic;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Enum;

namespace DataValidation.Dal.Operations
{
    public class RecurringDataOperation : IDataOperation
    {
        private readonly int _numberOfRepetitions;

        public RecurringDataOperation(int numberOfRepetitions)
        {
            _numberOfRepetitions = numberOfRepetitions;
        }

        public List<Data> Execute(List<Data> datas)
        {
            var recurringDatas = new Queue<Data>();
            var previousData = new Data();
            foreach (Data data in datas)
            {
                if (data.Value.Equals(previousData.Value))
                {
                    recurringDatas.Enqueue(previousData);
                }
                else
                {
                    if (recurringDatas.Count >= _numberOfRepetitions-1)
                    {
                        recurringDatas.Enqueue(previousData);
                        foreach (Data recurringData in recurringDatas)
                        {
                            recurringData.DataStatus = Status.Ponavljajoc;
                        }
                        recurringDatas.Clear();
                    }
                    else
                    {
                        recurringDatas.Clear();
                    }
                }
                previousData = data;
            }
            return datas;
        }
    }
}