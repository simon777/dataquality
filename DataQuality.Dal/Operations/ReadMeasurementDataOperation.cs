﻿using System.Collections.Generic;
using System.Linq;
using DataValidation.Dal.Entities;
using DataValidation.Dal.Entities.Datas;

namespace DataValidation.Dal.Operations
{
    public class ReadMeasurementDataOperation : IDataOperation
    {
        private readonly long _sourceId;

        public ReadMeasurementDataOperation(long sourceId)
        {
            _sourceId = sourceId;
        }

        public List<Data> Execute(List<Data> datas)
        {
            var context = new DataContext();
            IEnumerable<Data> enumerableDatas = 
                context.MeasurementDatasRaw.Where(data => data.MeasurementPointId.Equals(_sourceId)).OrderBy(data => data.DateInserted).ToList();
            return enumerableDatas.ToList(); 
           // return context.MeasurementDatasRaw.Where(data => data.Equals(_sourceId)).OrderBy(data => data.DateInserted).Cast<Data>().ToList(); // Cast Error !!
        }
    }
}