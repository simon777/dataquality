﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using DataValidation.Dal.Entities;
using DataValidation.Dal.Entities.Datas;
using EntityFramework.BulkInsert.Extensions; 
using System.Transactions;
//using Joti.Entity.Bulk;

namespace DataValidation.Dal.Operations
{
    public class WriteMeasurementDataOperation : IDataOperation
    {
        public List<Data> Execute(List<Data> datas)
        {
            var context = new DataContext();

            IEnumerable<MeasurementData> writeData = datas.Select(select => new MeasurementData()
            {
                MeasurementPointId = select.SourceId,
                Value = select.Value,
                DataStatus = select.DataStatus,
                DateInserted = select.DateInserted,
                UtcTimeStamp = select.UtcTimeStamp

            });



            DataTable t = new DataTable();
            t.Columns.Add("UserId", typeof(int));
            t.Columns.Add("MeasurementPointId", typeof(int));
            t.Columns.Add("UtcTimeStamp", typeof(DateTime));
            t.Columns.Add("Revision", typeof(int));
            t.Columns.Add("Value", typeof(decimal));
            t.Columns.Add("DateInserted", typeof(DateTime));

            t.Columns.Add("DataStatus", typeof(int));


            foreach (MeasurementData m in writeData)
            {
                if (m != null)
                {
                    t.Rows.Add(new object[]
                    {
                        m.UserId,
                        m.MeasurementPointId,
                        m.UtcTimeStamp,
                        m.Revision,
                        m.Value,
                        m.DateInserted,
                        m.DataStatus
                    });
                }

            }

            using (var bulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["TestDatabase"].ConnectionString,
                SqlBulkCopyOptions.KeepIdentity))
            {
                bulk.ColumnMappings.Clear();
                bulk.DestinationTableName = "MeasurementDatas";

                bulk.ColumnMappings.Add("UserId", "UserId");
                bulk.ColumnMappings.Add("MeasurementPointId", "MeasurementPointId");
                bulk.ColumnMappings.Add("UtcTimeStamp", "UtcTimeStamp");
                bulk.ColumnMappings.Add("Revision", "Revision");
                bulk.ColumnMappings.Add("Value", "Value");
                bulk.ColumnMappings.Add("DateInserted", "DateInserted");
                bulk.ColumnMappings.Add("DataStatus", "DataStatus");

                bulk.WriteToServer(t);
            }
            
            
            
            
            
            
            
            
            
            
            
            
            //var l1 = datas.Cast<MeasurementData>();

            //  context.BulkInsert(writeData);





            ////EntityFramework.BulkInsert.ProviderFactory.Register<System.Data.SqlClient>("System.Data.SqlClient.SqlConnection");
            //using (var context = new DataContext())
            //{


            //    //using (var transaction = new TransactionScope())
            //    //{
            //    //    context.MeasurementDatas.AddRange(writeData);
            //    //    context.SaveChanges();
            //    //    transaction.Complete();
            //    //}

            //}

           return writeData.Cast<Data>().ToList(); 
        }
    }
}