﻿namespace DataValidation.Dal.Enum
{
    public enum DataType
    {
        Measurement = 1,
        Product = 2,
        Weather = 3,
    }
}