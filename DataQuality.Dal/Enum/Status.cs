﻿namespace DataValidation.Dal.Enum
{
    public enum Status
    {
        Nevelidiran,
        Validiran,
        Manjkajoc,
        Popravljen,
        Napaka,
        RocnoVnesen,
        Ponavljajoc
    }
}