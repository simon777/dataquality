﻿namespace DataValidation.Dal.Enum
{
    public enum DataOperationType
    {
        Undefined = 1,
        Validation = 2,
        Aggregation = 3,
        Replacement = 4
    }
}