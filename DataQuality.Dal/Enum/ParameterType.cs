﻿namespace DataValidation.Dal.Enum
{
    public enum ParameterType
    {
        Integer= 0,
        DateTime = 1,
        Decimal = 2,
        Bit = 3,
        String = 4
    }
}