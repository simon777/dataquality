﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using DataValidation.Dal.Entities;

namespace DataValidation.Dal
{
    public class MissingData
    {
        public static void Run()
        {
            var stopwatch = new Stopwatch();
            var context = new DataContext();
            List<Data> dataList = context.Datas.Where(data => data.SifraMeritve.Equals(0)).OrderBy(data => data.InserdDateTime).ToList();
            DateTime nextDateTime = dataList.First().InserdDateTime;
            
            Console.WriteLine("Start Missing Data (ForEach)");
            stopwatch.Start();
            
            foreach (Data data in dataList)
            {
                if (!data.InserdDateTime.Equals(nextDateTime))
                {
                    do
                    {
                        Console.WriteLine("Meritev:{0} Cas:{1}", data.SifraMeritve, data.InserdDateTime);
                        nextDateTime = nextDateTime.AddMinutes(15);
                    } while (!data.InserdDateTime.Equals(nextDateTime)); 
                }

                nextDateTime = nextDateTime.AddMinutes(15);
            }

            stopwatch.Stop();
            Console.WriteLine("Missing Data Time elapsed (ForEach): {0}", stopwatch.Elapsed);
            stopwatch.Reset();
           
            
            
            Console.WriteLine("Start Missing Data (Enumerator)");
            stopwatch.Start();
            
            List<Data>.Enumerator dataListEnumerator = dataList.GetEnumerator();
            nextDateTime = dataList.First().InserdDateTime; 
            Data dataEnumerator = null;
            dataListEnumerator.MoveNext();
            do
            {
                dataEnumerator = dataListEnumerator.Current;
                if (!dataEnumerator.InserdDateTime.Equals(nextDateTime))
                {
                    Console.WriteLine("Meritev:{0} Cas:{1}", dataEnumerator.SifraMeritve,dataEnumerator.InserdDateTime);
                    nextDateTime = nextDateTime.AddMinutes(15); 
                }
                else
                {
                    nextDateTime = nextDateTime.AddMinutes(15); 
                    if (!dataListEnumerator.MoveNext()) break;
                }
            } while (true);

            stopwatch.Stop();
            Console.WriteLine("Missing Data Time elapsed (Enumerator): {0}", stopwatch.Elapsed);
        }
    }
}