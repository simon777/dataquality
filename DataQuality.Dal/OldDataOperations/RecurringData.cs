﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DataValidation.Dal.Entities;

namespace DataValidation.Dal
{
    public class RecurringData
    {
        public static void Run()
        {
            var stopwatch = new Stopwatch();
            var context = new DataContext();
            
            Console.WriteLine("Start Data Read");
            stopwatch.Start();
            List<Data> dataList = context.Datas.Where(data => data.SifraMeritve <50000).OrderBy(data => data.InserdDateTime).ToList();
            stopwatch.Stop();
            Console.WriteLine("Data read Time elapsed: {0}", stopwatch.Elapsed);
            stopwatch.Reset();
            // List<Data> datasListAsnc = await (context.Datas.Where(data => data.SifraMeritve < 3000).ToListAsync());
            // HashSet<Data> datasHashSet = context.Datas.Where(data => data.SifraMeritve < 1).ToListAsync();
            int numberOrRecurringDatas = 4;
            var queue = new Queue<Data>();
            int previousValue = 0;
            List<Data> changedDatas= new List<Data>();
            Console.Write("Start Recurring Validation");
            stopwatch.Start();
            foreach (Data data in dataList)
            {
                if (data.Value.Equals(previousValue))
                {
                    queue.Enqueue(data);
                }
                else
                {
                    if (queue.Count >= numberOrRecurringDatas)
                    {
                        foreach (Data dataChange  in queue)
                        {
                            dataChange.Status = true;
                        }
                        changedDatas.AddRange(queue);
                        queue.Clear();
                        //context.SaveChanges(); 
                    }
                    else
                    {
                        queue.Clear();
                    }
                }
                previousValue = data.Value;
            }
            stopwatch.Stop();
            Console.WriteLine("Recurring Validation Time elapsed: {0}", stopwatch.Elapsed);
            
            //Izpis spremenjenih 
            //foreach (Data data in changedDatas)
            //{
            //    Console.WriteLine(data.Value.ToString()+"-"+data.InserdDateTime.ToString());
            //}
            Console.WriteLine("Sum all: "+changedDatas.Count);

        }
    }
}