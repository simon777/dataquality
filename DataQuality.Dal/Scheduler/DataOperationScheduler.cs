﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataValidation.Dal.Entities.DataOperation;
using DataValidation.Dal.Enum;
using DataValidation.Dal.Factory;
using DataValidation.Dal.Operations;

namespace DataValidation.Dal.Scheduler
{
    public class DataOperationScheduler
    {
        public static List<DataOperation> GetOperationsForMeasurement(long measurementId)
        {
            using (var context = new DataContext())
            {
                DataOperationGroup dataOperationGroup = context.MeasurementPoints.Where(mp => mp.Id.Equals(measurementId))
                    .Select(mp => mp.MeasurementPointDataOperationGroups.FirstOrDefault())
                    .FirstOrDefault();
                if (dataOperationGroup != null)
                    return
                        context.DataOperations.Include(p => p.ParameterDatas.Select(select => select.DataOperationMethodParameter))
                            .Include(p => p.DataOperationMethod)
                            .Where(operation => operation.DataOperationGroupId == dataOperationGroup.Id)
                            .ToList();
            }
            return null;
        }

        public static List<IDataOperation> GetListOfDataOperations(DataType dataType, long sourceId)
        {
            var dataOperations = new List<IDataOperation>();
            List<DataOperation> dataValidationOperations = GetOperationsForMeasurement(1);

            //Read 
            dataOperations.Add(DataOperationFactory.GetReadOperation(dataType, sourceId));

            //Validate
            foreach (DataOperation dataValidationOperation in dataValidationOperations)
            {
                dataOperations.Add(DataOperationFactory.GetValidationOperation(dataValidationOperation.DataOperationMethod.Name, dataValidationOperation.ParameterDatas));
            }
            //Write
            dataOperations.Add(new WriteMeasurementDataOperation());
            return dataOperations;
        }
    }
}