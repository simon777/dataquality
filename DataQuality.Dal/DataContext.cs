﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using DataValidation.Dal.Entities.DataOperation;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Entities.Measurements;
using Joti.Entity.Bulk;

namespace DataValidation.Dal
{
    internal class DataContext : DbContext, IDbContextBulk
    {
        public DataContext() : base("name=TestDatabase")
        {
            Database.CommandTimeout = 5000;
        }

        public DbSet<MeasurementData> MeasurementDatas { get; set; }
        public DbSet<MeasurementDataRaw> MeasurementDatasRaw { get; set; }
        public DbSet<ProductData> ProductDatas { get; set; }
        public DbSet<ProductDataRaw> ProductDatasRaw { get; set; }
        public DbSet<WeatherData> WeatherDatas { get; set; }
        public DbSet<WeatherDataRaw> WeatherDatasRaw { get; set; }

        public DbSet<DataOperation> DataOperations { get; set; }
        public DbSet<DataOperationGroup> DataOperationGroups { get; set; }
        public DbSet<DataOperationMethod> DataOperationMethods { get; set; }
        public DbSet<DataOperationMethodParameter> DataOperationMethodParameters { get; set; }
        public DbSet<DataOperationMethodParameterData> DataOperationMethodParameterDatas { get; set; }

        public DbSet<MeasurementPoint> MeasurementPoints { get; set; }
        public DbSet<EnergyMeasurementPoint> EnergyMeasurementPoints { get; set; }
        public DbSet<EnergyMeasurementPointType> EnergyMeasurementPointTypes { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MeasurementDataRaw>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            modelBuilder.Entity<ProductDataRaw>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            modelBuilder.Entity<WeatherDataRaw>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            modelBuilder.Entity<MeasurementPoint>().ToTable("MeasurementPoints");
            modelBuilder.Entity<EnergyMeasurementPoint>().ToTable("MeasurementPointsEnergy");

            modelBuilder.Entity<MeasurementData>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<DataOperation>()
             .HasRequired(dogm => dogm.DataOperationMethod)
             .WithMany()
             .HasForeignKey(dogm => dogm.DataOperationMetodId)
             .WillCascadeOnDelete(false);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}