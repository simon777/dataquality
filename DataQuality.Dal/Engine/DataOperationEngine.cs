﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using DataValidation.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Enum;
using DataValidation.Dal.Operations;
using DataValidation.Dal.Scheduler;
using EntityFramework.BulkInsert.Extensions;

namespace DataValidation.Dal.Engine
{
    public class DataOperationEngine
    {
        public static void Run()
        {
            var context = new DataContext();
            List<long> sourceIds = context.MeasurementDatasRaw.GroupBy(data => data.MeasurementPointId).Select(data => data.FirstOrDefault().MeasurementPointId).ToList();
            // List<long> sourceIds = new List<long>();
           // sourceIds.Add(1);

            List<Data> datas1= new List<Data>();
            ReadMeasurementDataOperation read= new ReadMeasurementDataOperation(10);
            datas1= read.Execute(datas1);

            List<Data> l = new List<Data>();
            l.Clear();


            
            Parallel.ForEach(sourceIds, (sourceId) =>
            {
                List<Data> datas = new List<Data>();
                List<IDataOperation> dataOperations = DataOperationScheduler.GetListOfDataOperations(DataType.Measurement,sourceId);
                foreach (IDataOperation dataOperation in dataOperations)
                {
                    datas = dataOperation.Execute(datas);
                   //  if (dataOperation is WriteMeasurementDataOperation)
                   //      l.AddRange(datas);
                }
            });
           
            
            var count1 =l.Count(); 
            var l1 = l.Cast<MeasurementData>();
            var count2 = l1.Count();


            DataTable t = new DataTable();
            t.Columns.Add("UserId", typeof (int));
            t.Columns.Add("MeasurementPointId", typeof (int));
            t.Columns.Add("UtcTimeStamp", typeof (DateTime));
            t.Columns.Add("Revision", typeof (int));
            t.Columns.Add("Value", typeof(decimal));
            t.Columns.Add("DateInserted", typeof (DateTime));

            t.Columns.Add("DataStatus", typeof (int));


            foreach (MeasurementData m in l1)
            {
                if (m != null)
                {
                    t.Rows.Add(new object[]
                    {
                        m.UserId,
                        m.MeasurementPointId,
                        m.UtcTimeStamp,
                        m.Revision,
                        m.Value,
                        m.DateInserted,
                        m.DataStatus
                    });
                }

            }

            using (var bulk = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["TestDatabase"].ConnectionString, 
                SqlBulkCopyOptions.KeepIdentity))
            {
                bulk.ColumnMappings.Clear();
                bulk.BatchSize 
                bulk.DestinationTableName = "MeasurementDatas";

                bulk.ColumnMappings.Add("UserId", "UserId");
                bulk.ColumnMappings.Add("MeasurementPointId", "MeasurementPointId");
                bulk.ColumnMappings.Add("UtcTimeStamp", "UtcTimeStamp");
                bulk.ColumnMappings.Add("Revision", "Revision");
                bulk.ColumnMappings.Add("Value", "Value");
                bulk.ColumnMappings.Add("DateInserted", "DateInserted");
                bulk.ColumnMappings.Add("DataStatus", "DataStatus");

                //bulk.WriteToServer(t);
            }
            context.BulkInsert(l1);
            //context.SaveChanges();

        }
    }
}
