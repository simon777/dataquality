﻿using System;
using System.Collections.Generic;
using DataValidation.Dal.Entities;
using DataValidation.Dal.Entities.Datas;
using DataValidation.Dal.Enum;
using Joti.Entity.Bulk;

//using EntityFramework.BulkInsert.Extensions; 


namespace DataValidation.Dal
{
    public class TestData
    {
        public static void InsertTestData()
        {
            var context = new DataContext(); 
            //MeasurementData
            var startDateTime = new DateTime(2015, 1, 1, 0, 0, 0);
            DateTime endDateTiem = startDateTime.AddDays(30);
            const Status status = Status.Nevelidiran;
            var rnd = new Random();
            var data = new List<MeasurementDataRaw>();
            int id = 0;
            for (int i =0 ; i < 1000; i++)
            {

                startDateTime = new DateTime(2015, 1, 1, 0, 0, 0);
                while (startDateTime.Date < endDateTiem)
                {
                    var testData = new MeasurementDataRaw
                    {
                        Id = id,
                        UtcTimeStamp = startDateTime,
                        DateInserted = startDateTime,
                        DataStatus = status,
                        Value = rnd.Next(70),
                        MeasurementPointId = i
                    };
                    data.Add(testData);
                    id++;
                    startDateTime = startDateTime.AddMinutes(15);
                }
            }
            context.BulkInsert(data);
        }
    }
}